/* eslint-disable no-undef */
export default function({
    mathEditor,
    locale,
    title,
    description,
    shortcuts,
    answerTitle,
    updated,
    startedAt,
    sendFeedback,
    langLink,
    langLabel,
    
    importTitle,
    exportTitle,
    pdfExportTitle,
    nameLabel
}) {
    return `
<html>
<head>
    <meta charset='utf-8'>
    <title>${mathEditor}</title>
    <link rel="stylesheet" type="text/css" href="/mathquill/build/mathquill.css">
    <link rel="stylesheet" type="text/css" href="/rich-text-editor.css"/>
    <link rel="stylesheet" type="text/css" href="/student.css"/>
    <script src="/jquery/dist/jquery.js"></script>
    <script src="/baconjs/dist/Bacon.js"></script>
    <script src="/mathquill/build/mathquill.js"></script>
    <script type="text/javascript" src="/mathjax/MathJax.js"></script>
    <link rel="icon" href="/rich-text-editor-favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="/rich-text-editor-favicon.ico" type="image/x-icon"/>
    <script>
        window.locale = '${locale}'
    </script>
</head>
<body>
<article>
    <section>
        <h1>${title}</h1>

        <div class="instructions">
            <div style="width: 55%">
                ${description}
                <div><input  type="file" id="input"></div>
                <div><label for="input" class="button" id="inputLabel">${importTitle}</label></div>
                <div><a download="math-demo-answer.html" class="button" id="export" href="javascript:void(0)">${exportTitle}</a></div>
                <button type="button" onclick="exportPDF()" class="button">${pdfExportTitle}</button>
            </div>
            <div style="width:45%">
                ${shortcuts}
            </div>
        </div>

        <h2>${answerTitle}</h2>
        <label for="name">${nameLabel}</label>
        <input id="name" type="text" />
        <div class="answer" id="answer1"></div>
    </section>
</article>
<footer>
    <section>
        <div class="paragraph">
            ${updated} ${startedAt}
        </div>
        <div class="paragraph">
            <a href="https://gitlab.com/pekkarr/rich-text-editor-extended">GitLab</a>
        </div>
        <div class="paragraph">
            <a href="mailto:abitti-palaute@ylioppilastutkinto.fi?subject=Palaute / Math-editor">${sendFeedback}
                (abitti-palaute@ylioppilastutkinto.fi)</a>
        </div>
        <div class="paragraph">
            <a href="${langLink}">${langLabel}</a>
        </div>
    </section>
</footer>
<script src="/rich-text-editor-bundle.js"></script>
<script src="/student.js"></script>
</body>
</html>`
}
