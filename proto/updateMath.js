/* global MathJax */
import $ from 'jquery'
import { Base64 } from 'js-base64'
import errorSVG from './error.svg'
let math = null
let $result
export const init = () => {
    $result = $('<div class="result">\\({}\\)</div>')
    $('body').append($result)
    MathJax.Hub.Config({
        jax: ['input/TeX', 'output/SVG'],
        extensions: [
            'toMathML.js',
            'tex2jax.js',
            'MathMenu.js',
            'MathZoom.js',
            'fast-preview.js',
            'AssistiveMML.js',
            'a11y/accessibility-menu.js'
        ],
        TeX: {
            extensions: ['AMSmath.js', 'AMSsymbols.js', 'noErrors.js', 'noUndefined.js']
        },
        SVG: {
            useFontCache: true,
            useGlobalCache: false,
            EqnChunk: 1000000,
            EqnDelay: 0
        },
        messageStyle: 'none'
    })
    MathJax.Hub.queue.Push(() => {
        math = MathJax.Hub.getAllJax('MathOutput')[0]
    })
    MathJax.Hub.Queue(function() {
        MathJax.Hub.getAllJax(document.querySelector('.result'))
    })
}

const asBase64Svg = xml => 'data:image/svg+xml;base64,' + Base64.encode(xml)

export const updateMath = latex => new Promise(resolve => {
    MathJax.Hub.queue.Push(['Text', math, '\\displaystyle{' + latex + '}'])
    MathJax.Hub.Queue(() => {
        const $svg = $result.find('svg')
        if ($svg.length) {
            $svg.attr('xmlns', 'http://www.w3.org/2000/svg')
                .find('use')
                .each(function() {
                    const $use = $(this)
                    if ($use[0].outerHTML.indexOf('xmlns:xlink') === -1) {
                        $use.attr('xmlns:xlink', 'http://www.w3.org/1999/xlink') //add these for safari
                    }
                })
            let svgHtml = $svg.prop('outerHTML')
            svgHtml = svgHtml.replace(' xlink=', ' xmlns:xlink=') //firefox fix
            svgHtml = svgHtml.replace(/ ns\d+:href/gi, ' xlink:href') // Safari xlink ns issue fix
            resolve(asBase64Svg(svgHtml))
        } else {
            resolve(asBase64Svg(errorSVG))
        }
    })
})
