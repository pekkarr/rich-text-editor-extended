import $ from 'jquery'
import { Base64 } from 'js-base64'
import html2pdf from 'html2pdf.js'
import { makeRichText } from '../src/rich-text-editor'
import * as updateMath from './updateMath'

const exportPDF = () => {
    const source = document.getElementById('answer1')
    const name = $('#name').val()
    source.style.borderWidth = 0
    const options = {
        margin: 10,
        filename: `${name.length !== 0 ? name : 'document'}.pdf`,
        image: {
            type: 'png'
        },
        html2canvas: {
            scale: 2
        }
    }
    html2pdf().set(options).from(source).save().then(() => {
        source.style.borderWidth = null
    })
}
window.exportPDF = exportPDF

updateMath.init()

const answer = document.getElementById('answer1')
makeRichText(answer, {
    screenshot: {
        saver: ({ data }) =>
            new Promise(resolve => {
                const reader = new FileReader()
                reader.onload = evt => resolve(evt.target.result)
                reader.readAsDataURL(data)
            }),
        limit: 10
    },
    baseUrl: '',
    updateMathImg: ($img, latex) => {
      updateMath.updateMath(latex).then(svg => {
        $img.prop({
          src: svg,
          alt: latex
        })
        $img.closest('[data-js="answer"]').trigger('input')
      })
    }
})
answer.focus()

const trackError = (e = {}) => {
}

if (window.addEventListener) {
    window.addEventListener('error', trackError, false)
} else if (window.attachEvent) {
    window.attachEvent('onerror', trackError)
} else {
    window.onerror = trackError
}


// import and export functionality

const reader = new FileReader()
reader.onload = x => {
    const document = JSON.parse(x.target.result)
    switch (document.version) {
        case '0.0.1':
            $('.answer').html(document.answer)
            break
        default:
            window.alert(`Document version ${document.version} is not supported`)
    }
}

const fileInputElement = document.getElementById("input")

const handleFiles = () => {
    const file = fileInputElement.files[0]
    reader.readAsText(file)
}

fileInputElement.addEventListener("change", handleFiles, false)

$('#export').click(e => {
    const document = {
        version: '0.0.1',
        answer: $('.answer').html()
    }
    const link = 'data:application/json;base64,' + Base64.encode(JSON.stringify(document))
    let name = $('#name').val()
    if (name.length === 0) name = 'math-demo-answer-' + new Date().toISOString().split('.')[0]
    $(e.target)
        .attr('href', link)
        .attr('download', name + '.math')

})

const previousAnswer = localStorage.getItem('answer')
if (previousAnswer)
    $('.answer').html(previousAnswer)
