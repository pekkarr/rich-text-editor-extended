import express from 'express'
import enforce from 'express-sslify'

const app = express()

if (process.env.ENVIRONMENT === 'heroku') {
  console.log('Enforcing HTTPS')
  app.use(enforce.HTTPS({ trustProtoHeader: true }))
}

process.on('uncaughtException', function(err) {
  console.log('Uncaught exception: ', err.stack)
  process.exit(1)
})

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept')
  next()
})

app.use(express.static(__dirname + '/../site'))

// eslint-disable-next-line no-unused-vars
app.use((error, req, res, next) => {
  console.log('Request error: ', error.stack)
  res.status(500).end()
})

module.exports = app
